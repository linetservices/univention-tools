#!/bin/bash

tmpf=$(mktemp)

trap "rm -f $tmpf" EXIT

function l {
  logger --id=$$ -t "linet-dhcp-fixed-addresses" "$@"
}

l "Looking for DHCP host entries with empty univentionDhcpFixedAddress attributes"

univention-ldapsearch \
  -LLLo ldif-wrap=no \
  "(&(univentionObjectType=dhcp/host)(univentionDhcpFixedAddress=))" dn | \
  grep '^dn:' | \
  sed -e 's/^dn: *//' | {
  while read dn ; do
    cat >> ${tmpf} <<EOF
dn: ${dn}
changetype: modify
delete: univentionDhcpFixedAddress

EOF
  done
}

if [[ ! -s ${tmpf} ]]; then
  l "Nothing found that needs fixing."
  exit 0
fi

num_entries=$(grep '^dn:' ${tmpf} | wc -l | awk '{ print $1 }')
l "Fixing ${num_entries} entries"

output=$(ldapmodify -D cn=admin,$(ucr get ldap/base) -y /etc/ldap.secret -x -f ${tmpf} 2>&1)
code=$?

if [[ ${code} != 0 ]]; then
  l "Fixing failed (exit code ${code}): ${output}"
  exit ${code}
fi

l "Fixing succeeded."
