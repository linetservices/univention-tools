# A tool fixing DHCP host entries with empty fixed address attributes

## Introduction

There's a bug in at least UCS 4.4-0 & 4.4-1 that creates DHCP host
entries where the `univentionDhcpFixedAddress` attribute is present
but empty. This can be triggered as follows:

1. Log in to the UMC
2. Edit a computer object
3. Make sure a MAC address is configured
4. In the DHCP section add an entry with the MAC address & the DHCP
   network set, but no IP address set
5. Save the object

Such a configuration can be used in combination with setting the DHCP
server to only serve addresses to known clients (= known MAC
addresses). That way the server will assign random IPs from its pool,
but only to MAC addresses of existing DHCP host entries.

The problem is that the UMC creates a DHCP host entry where the
`univentionDhcpFixedAddress` attribute, which is supposed to contain
the IP address for static mappings, is present but empty. The DHCP
server in turn interprets that as the configuration directive
`fixed-address ;` which is invalid as `fixed-address` must be followed
by an IP address.

This script "fixes" such DHCP host entries by removing the empty
`univentionDhcpFixedAddress` attributes.

## Usage

Run the script as `root` on your DC master. Ideally run it as a CRON
job.

The script logs via syslog.

## Copyright

Copyright (c) 2019 Moritz Bunkus <m.bunkus@linet-services.de>

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](GPL-2.txt) for more details.
