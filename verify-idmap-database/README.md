# A tool verifying entries in idmap.ldb

## Introduction

This tool reads user & group information from the OpenLDAP server and
compares the information to Samba's ID mapping database,
`/var/lib/samba/private/idmap.ldb`. For each user and group it
verifies that there's an entry in the ID mapping database for the
corresponding user/group ID and that the mapping resolves to the
correct SID.

## Usage

Simply run the script. I'll output one line for each user/group with
the result. Lines will be prefixed with either `ERROR:` or `OK:`.

The script will exit with code 1 if at least one error is found and
with code 0 if all entries are OK.

## Copyright

Copyright (c) 2019 Moritz Bunkus <m.bunkus@linet-services.de>

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](GPL-2.txt) for more details.
