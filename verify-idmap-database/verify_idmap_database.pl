#!/usr/bin/perl

use strict;
use warnings;

my (%idmap, @openldap, $has_errors);

sub parse_ldif {
  my (@lines) = @_;

  my (%attrs, @entries);

  foreach my $line (@lines) {
    chomp $line;
    my ($key, $value);

    if ($line =~ m{^([^:]+):+ +(.+)}) {
      $attrs{lc $1} //= [];
      push @{ $attrs{lc $1} }, $2;
      next;
    }

    if ($line eq '') {
      push @entries, { %attrs } if %attrs;
      %attrs = ();
    }
  }

  return @entries;
}

sub read_idmap {
  my $command = "ldbsearch -H /var/lib/samba/private/idmap.ldb";
  my @entries = parse_ldif(split m{\n}, `$command`);

  foreach my $entry (@entries) {
    next unless $entry->{type} && $entry->{xidnumber} && $entry->{objectsid};

    if ($entry->{type}->[0] =~ m{ID_TYPE_(UID|BOTH)}) {
      $idmap{"uidnumber:" . $entry->{xidnumber}->[0]} = $entry->{objectsid}->[0];
    }

    if ($entry->{type}->[0] =~ m{ID_TYPE_(GID|BOTH)}) {
      $idmap{"gidnumber:" . $entry->{xidnumber}->[0]} = $entry->{objectsid}->[0];
    }
  }
}

sub read_openldap {
  my $command = "univention-ldapsearch"
    . " -o ldif-wrap=no"
    . " '(&(|(objectclass=posixaccount)(objectclass=posixgroup))(sambasid=*))'"
    . " dn sambasid uidnumber gidnumber objectClass";
  @openldap = parse_ldif(split m{\n}, `$command`);
}

sub verify_entries {
  foreach my $entry (@openldap) {
    next if !$entry->{dn};
    next if  $entry->{dn}->[0] =~ m{cn=builtin}i;
    next if !$entry->{objectclass} || !$entry->{sambasid};

    my $type = (grep { $_ eq 'posixAccount' } @{ $entry->{objectclass} }) ? 'uidnumber' : 'gidnumber';

    next unless $entry->{$type};

    my $index = "${type}:" . $entry->{$type}->[0];

    if (!exists $idmap{$index}) {
      $has_errors = 1;
      print "ERROR: DN has no entry in idmap: " . $entry->{dn}->[0] . "\n";

    } elsif ($idmap{$index} ne $entry->{sambasid}->[0]) {
      $has_errors = 1;
      print "ERROR: DN has entry in idmap with different SID: " . $entry->{dn}->[0] . "\n";

    } else {
      print "OK: DN present with corrent SID: " . $entry->{dn}->[0] . "\n";
    }
  }
}

read_idmap();
read_openldap();
verify_entries();

exit($has_errors ? 1 : 0);
