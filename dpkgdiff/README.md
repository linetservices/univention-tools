# A simple dpkg-dist/dpkg-old updater based on pacdiff

This is an adaptation of Arch Linux' `pacdiff` tool for Debian-based
systems.

## Copyright

Copyright (c) 2007 Aaron Griffin <aaronmgriffin@gmail.com>
Copyright (c) 2013-2016 Pacman Development Team <pacman-dev@archlinux.org>
Copyright (c) 2016 Moritz Bunkus <moritz@bunkus.org>

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](GPL-2.txt) for more details.
