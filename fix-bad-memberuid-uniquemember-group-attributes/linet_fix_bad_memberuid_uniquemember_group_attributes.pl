#!/usr/bin/perl

use strict;
use warnings;

use utf8;

use FindBin;
use lib $FindBin::Bin;

use Data::Dumper;
use File::Temp;
use IO::File;
use MIME::Base64;

our (@entries, %existing_uids, %existing_user_dns, %changes);

sub read_file {
  my ($file_name) = @_;

  my (@lines, $line);

  my $in = IO::File->new($file_name, "<:encoding(utf-8)") || die("Could not open $file_name: $!");
  push @lines, $line while $line = <$in>;
  $in->close;

  return @lines;
}

sub read_ldif {
  my ($file_name) = @_;

  my @lines = read_file($file_name);
  chomp @lines;

  my (@entries, $entry);

  foreach my $line (@lines) {
    next if $line =~ m{^$|^#};
    next if $line !~ m{^([^:]+):(:)? +(.+)};

    my ($key, $is_base64, $value) = (lc($1), $2, $3);
    $value                        = decode_base64($value) if $is_base64;

    if ($key eq 'dn') {
      $entry = {};
      push @entries, $entry;
    }

    $entry->{$key} //= [];
    push @{ $entry->{$key} }, $value;
  }

  return @entries;
}

sub ldif_line {
  my ($attribute, $value) = @_;

  my $need_base64 = $value =~ m{[^a-zA-Z0-9 _,=-]};

  return sprintf('%s%s: %s', $attribute, $need_base64 ? ':' : '', $need_base64 ? encode_base64($value) : $value);
}

sub handle_dangling_memberuids {
  foreach my $entry (@entries) {
    next unless $entry->{_is_group};
    next unless $entry->{memberuid};

    my $dn = lc $entry->{dn}->[0];

    foreach my $uid (@{ $entry->{memberuid} }) {
      next if lc($uid) eq 'krbtgt';
      next if $existing_uids{lc($uid)};

      $changes{$dn} //= [];
      push @{ $changes{$dn} }, sprintf("delete: memberUid\n\%s\n", ldif_line('memberUid', $uid));
    }
  }
}

sub handle_dangling_uniquemembers {
  foreach my $entry (@entries) {
    next unless $entry->{_is_group};
    next unless $entry->{uniquemember};

    my $dn = lc $entry->{dn}->[0];

    foreach my $unique_member_dn (@{ $entry->{uniquemember} }) {
      next if $existing_user_dns{lc($unique_member_dn)};
      next if lc($unique_member_dn) !~ m{^uid=([^,]+)};

      my $uid = $1;
      next if lc($uid) eq 'krbtgt';

      $changes{$dn} //= [];
      push @{ $changes{$dn} }, sprintf("delete: uniqueMember\n\%s\n", ldif_line('uniqueMember', $unique_member_dn));

      if ($existing_uids{lc $uid} && !grep { lc($_) eq lc($existing_uids{lc $uid}) } @{ $entry->{uniquemember} }) {
        push @{ $changes{$dn} }, sprintf("add: uniqueMember\n\%s\n", ldif_line('uniqueMember', $existing_uids{lc $uid}));
      }
    }
  }
}

sub handle_uniquemembers_without_memberuids {
  foreach my $entry (@entries) {
    next unless $entry->{_is_group};
    next unless $entry->{uniquemember};

    my $dn = lc $entry->{dn}->[0];

    foreach my $unique_member_dn (@{ $entry->{uniquemember} }) {
      next if lc($unique_member_dn) !~ m{^uid=([^,]+)};

      my $uid = $1;

      next if grep { lc($_) eq lc($uid) } @{ $entry->{memberuid} // [] };

      $changes{$dn} //= [];
      push @{ $changes{$dn} }, sprintf("add: memberUid\n\%s\n", ldif_line('memberUid', $uid));
    }
  }
}

sub show_changes {
  foreach my $dn (sort { lc $a cmp lc $b } keys %changes) {
    printf "\%s\n\%s\n\n", ldif_line('dn', $dn), join("-\n", @{ $changes{$dn} });
  }
}

sub setup {
  $Data::Dumper::Sortkeys = 1;
  $Data::Dumper::Indent   = 2;

  binmode STDIN,  ':utf8';
  binmode STDOUT, ':utf8';
  binmode STDERR, ':utf8';

  if (@ARGV) {
    @entries = read_ldif($ARGV[0]);

  } else {
    my $temp_file = File::Temp->new;
    $temp_file->close;

    my $result = system "univention-ldapsearch '(|(uid=*)(uniquemember=*)(memberuid=*))' uid uniquemember memberuid objectclass > " . $temp_file->filename;

    die "LDAP search failed" if $result != 0;

    @entries = read_ldif($temp_file->filename);
  }

  %existing_uids     = map { (lc($_->{uid}->[0]) => $_->{dn}->[0]) } grep { $_->{uid} } @entries;
  %existing_user_dns = map { (lc($_->{dn}->[0])  => $_->{dn}->[0]) } grep { $_->{uid} } @entries;

  foreach my $entry (@entries) {
    $entry->{_is_group} = 1 if grep { lc($_) eq 'posixgroup' } @{ $entry->{objectclass} };
  }
}

setup();
handle_dangling_memberuids();
handle_dangling_uniquemembers();
handle_uniquemembers_without_memberuids();
show_changes();
