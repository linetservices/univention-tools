# A tool fixing bad `uniqueMember` and `memberUid` group attributes

## Introduction

Group membership is stored via two attributes in the OpenLDAP server:

* the `uniqueMember` attribute contains the DN of the user object
* the `memberUid` attribute contains the `uid` attribute of the user object

Both must be present. Under certain conditions several things can
happen with those attributes:

1. `uniqueMember` might contain a DN, but there's no such object with
   that DN anymore: the user object was removed, but the group wasn't
   updated.

2. `memberUid` might contain a UID, but there's no corresponding
   object with that UID anymore: the user object was removed, but the
   group wasn't updated.

3. `uniqueMember` might contain a DN, but there's no such object with
   that DN anymore. However, there might be another DN for the same
   UID: the user object was moved, but the group wasn't updated.

4. `uniqueMember` might contain a DN, but the group doesn't contain a
   `memberUid` attribute with the DN's UID.

Those cases lead to various issues in UCS management systems. For
example case 4 might lead to errors being thrown when trying to change
group membership via the Univention Management Console.

This script searches the LDAP directory for groups having one or more
of the aforementioned problems. It'll output an LDIF that fixes the
issues. In particular, it does the following:

1. `uniqueMember` attributes whose DNs don't exist anymore and for
   which there is no other DN with the same UID will be removed from
   the group.

2. `memberUid` attributes for which no object with the same UID exists
   anymore will be removed from the group.

3. `uniqueMember` attributes whose DNs don't exist anymore but for
   which there is another DN with the same UID will be changed to
   point to the new UID.

4. For each `uniqueMember` for which no corresponding `memberUid`
   exists in the same group a matching `memberUid` attribute will be
   added.

## Usage

Run the script as `root` on your DC master. Redirect the output to an
LDIF file. Take a good look at the LDIF file & decide whether or not
the change are OK. Apply the changes with `ldapmodify`.

Example:

```sh
linet_fix_bad_memberuid_uniquemember_group_attributes.pl > fixes.ldif
ldapmodify -D cn=admin,$(ucr get ldap/base) -y /etc/ldap.secret fixes.ldif
```

## Copyright

Copyright (c) 2020 Moritz Bunkus <m.bunkus@linet.de>

## License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU General Public License](GPL-2.txt) for more details.
